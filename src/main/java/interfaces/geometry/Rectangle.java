package interfaces.geometry;

public class Rectangle implements Shape{
    private double length;
    private double width;

    public Rectangle(double length, double width){
        this.length = length;
        this.width = width;
    }

    public double getLength(){
        return this.length;
    }

    public double getWidth(){
        return this.width;
    }

    public double getArea(){
        double area = this.length * this.width;
        return area;
    }

    public double getPerimeter(){
        double perimeter = (2 * this.length) + (2 * this.width);
        return perimeter;
    }
}

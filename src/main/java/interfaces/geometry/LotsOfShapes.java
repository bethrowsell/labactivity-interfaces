package interfaces.geometry;

public class LotsOfShapes {
    
    public static void main(String[] args){
        Shape[] shape = new Shape[5];
        shape[0] = new Rectangle(4, 6);
        shape[1] = new Rectangle(3, 5);
        shape[2] = new Circle(3);
        shape[3] = new Circle(2);
        shape[4] = new Square(5);

        for(Shape s : shape){
            System.out.println("Area: " + s.getArea() + ", Perimeter: " + s.getPerimeter());
        }
    }
}

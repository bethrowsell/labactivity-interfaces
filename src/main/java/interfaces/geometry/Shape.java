package interfaces.geometry;

public interface Shape {
    double getArea();
    double getPerimeter();
}